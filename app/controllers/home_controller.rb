class HomeController < ApplicationController
  layout 'game', only: [:game]

  def index
  end

  def termsandconditions
  end

  def casino
    render layout: 'casino'
  end

  def projectplay
    render layout: 'empty'
  end

  def game
    if user_signed_in?
    gon.user = current_user.id
    end
  end

  def activate
    @user = User.find(params[:id])

    if @user.update(active: params[:active])
      render json: {
          success: 'OK',
          message: 'Your did not win'
      }
    else
      render json: {
          error_message: @user.errors.full_messages
      }
    end
  end
    private
      def user_params
        params.require(:user).permit(:active)
      end
end
