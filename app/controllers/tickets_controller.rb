class TicketsController < ApplicationController
  before_action :authenticate_user!, except: [:ticket_check, :ticket_verify, :index, :import, :new, :create]

  def index
    @tickets = Ticket.all
    respond_to do |format|
      format.html
      format.csv { send_data @tickets.to_csv, filename: "tickets-#{Date.today}.csv" }
    end
  end

  def import
    Ticket.import_csv(params[:file])
    redirect_to tickets_path, notice: 'Data imported successfully.'
  end

  def ticket_check
  #   Create Checked
  #   Check if the ticket exists.
  #     Check the "checked parameter"
  #      if it is set to false or null set "Checked" to true and the then send message: "The ticket has been checked and validated" , code: 1
  #     If is is set to true then message: "Ticket already scanned. Duplicate Ticket", code : 2
  #  ELSE send message : "Ticket DOES NOT EXIT. FAKE Ticket!", code : 3
  #
    if params[:code].empty?
      render json: {
          code: 404,
          message: 'No PARAMS detected'
      }
    end
    ticket_code = params[:code]
    ticket = Ticket.find_by(random_code: ticket_code)

    if ticket.present?
      if ticket.checked
        render json: {
            code: 2,
            message: 'Ticket already scanned. DUPLICATE Ticket'
        }
      else
          ticket.update!(checked: true)
         render json: {
             code: 1,
             message: 'The ticket has been checked and validated.'
         }
      end

    else
      render json: {
          code: 3,
          message: 'FAKE TICKET! Ticket does not exist!'
      }
    end

  end


  def ticket_verify
    # This is the code to verify if the ticket is valid. And has been checked properly.
      if params[:code].empty?
        render json: {
            code: 404,
            message: 'No PARAMS detected'
        }
      end
      ticket_code = params[:code]
      ticket = Ticket.find_by(random_code: ticket_code)
  
      if ticket.present?
        if ticket.checked
          render json: {
              code: 4,
              message: 'Valid Ticket'
          }
        else
            # ticket.update!(checked: true)
           render json: {
               code: 5,
               message: 'INVALID TICKET'
           }
        end
  
      else
        render json: {
            code: 3,
            message: 'FAKE TICKET! Ticket does not exist!'
        }
      end
  
    end

  def vip_tickets
    @tickets = Ticket.where(ticket_type: 5)
  end

  def bob_tickets
    @tickets = Ticket.where(ticket_type: 6)
  end

  def adult_day_one
    @tickets = Ticket.where("ticket_type = 1 and student = 1")
  end

  # Total gen : 800, Checked : 798
  def adult_day_two
    @tickets = Ticket.where("ticket_type = 2 and student = 1")
  end

  # Gen: 1110, Checked : 1100
  def adult_season
    @tickets = Ticket.where("ticket_type = 3 and student = 1")
  end

  # Gen 800, Checked : 733
  def student_day1
    @tickets = Ticket.where(["ticket_type = 1 and student = 2"])
  end

  # Gen: 1500, Print: 1200, Checked: 1188
  def student_season
    @tickets = Ticket.where(["ticket_type = 3 and student = 2"]).limit(1200)
  end

  #  Gen 800, Checked : 790
  def student_day2
    @tickets = Ticket.where(["ticket_type = 2 and student = 2"])
  end

  #  Total printed tickets: 5510
  #   Total checked: 5207
  # False tickets : 303

  def new
    @ticket = Ticket.new
  end

  def create
    @ticket = Ticket.new(set_params)

    if @ticket.number_of_tickets > 0
      @ticket.number_of_tickets.times do
        Ticket.create!(random_code: ('a'..'z').to_a.shuffle[0,12].join, student: @ticket.student, ticket_type: @ticket.ticket_type, user_listed: @ticket.user_listed)
      end
    end
    redirect_to tickets_path, notice: 'The tickets have been generated'

  end


  private

  def set_params
    params.require(:ticket).permit(:user_listed, :student, :number_of_tickets, :ticket_type)
  end
end