let count = 0;
const btnShuffle = document.querySelector('#casinoShuffle');
const btnStop = document.querySelector('#casinoStop');
const casino1 = document.querySelector('#casino1');
const casino2 = document.querySelector('#casino2');
const casino3 = document.querySelector('#casino3');
const mCasino1 = new SlotMachine(casino1, {
    active: 0,
    delay: 500
});
const mCasino2 = new SlotMachine(casino2, {
    active: 1,
    delay: 500
});
const mCasino3 = new SlotMachine(casino3, {
    active: 2,
    delay: 500
});

btnShuffle.addEventListener('click', () => {
    count = 3;
    mCasino1.shuffle(9999);
    mCasino2.shuffle(9999);
    mCasino3.shuffle(9999);
    // element.remove();
    btnShuffle.remove();
});

btnStop.addEventListener('click', () => {
    switch(count) {
    case 3:
        mCasino1.stop();
        break;
    case 2:
        mCasino2.stop();
        break;
    case 1:
        mCasino3.stop();
        var a = mCasino1.visibleTile + mCasino2.visibleTile + mCasino3.visibleTile ;
        if (a < 15){
            Rails.ajax({
                url: "/home/activate/"+gon.user,
                type: "POST",
                data: "active=false",
                success: function(data) {
                    console.log(data);
                    window.location.reload();
                }
            });
        }else {
            alert('you won');
        }
        break;

    }
    count--;

});
