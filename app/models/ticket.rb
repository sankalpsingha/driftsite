# == Schema Information
#
# Table name: tickets
#
#  id                :bigint(8)        not null, primary key
#  random_code       :string(255)
#  user_listed       :string(255)
#  qr_code           :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  ticket_type       :integer
#  number_of_tickets :integer
#  student           :integer
#

require 'csv'

class Ticket < ApplicationRecord

  TICKET_TYPE = {
      1 => 'DAY 1',
      2 => 'DAY 2',
      3 => 'SEASON PASS',
      4 => 'FREE PASS',
      5 => 'VIP',
      6 => 'BOB' # Battle Of Bands
  }.freeze

  STUDENT = {
      1 => 'NON - STUDENT',
      2 => 'STUDENT'
  }.freeze

  # So that all the values that get generated is random
  validates :random_code, uniqueness: true

  validates :user_listed, :ticket_type, :student, presence: true

  mount_base64_uploader :qr_code, QrCodeUploader
  after_create :update_generate_code

  # This is for the importing of the CSV to the web app.
  def self.import_csv(file)
    items = []
    CSV.foreach(file.path, headers: true) do |row|
      items << Ticket.new(row.to_h)
    end
    Ticket.import(items)
  end

  def self.to_csv
    attributes = %w{id random_code user_listed ticket_type student checked}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

  def update_generate_code
    base_64_image = RQRCode::QRCode.new(self.random_code).as_png(
         size: 280
    ).to_data_url
    self.qr_code = base_64_image
    self.save!
  end

end
