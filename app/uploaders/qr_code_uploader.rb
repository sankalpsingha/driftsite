class QrCodeUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end


  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url(*args)
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end
  #
  #

  #
  # def maketicket
  #   second_image = MiniMagick::Image.open('http://localhost:3000/ticket.jpg')
  #   manipulate! do |img|
  #     second_image.composite(img) do |c|
  #       c.compose "Over" # OverCompositeOp
  #       # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
  #       c.geometry "+1575+130"
  #     end
  #   end
  #
  # end

  # process :maketicket


  version :day1 do
    process :maketicket_day1
  end


  version :day2 do
    process :maketicket_day2
  end


  version :day1_student do
    process :maketicket_student_day1
  end


  version :day2_student do
    process :maketicket_student_day2
  end


  version :maketicket_season do
    process :maketicket_season
  end



  version :maketicket_season_student do
    process :maketicket_student_season
  end

  version :vip do
    process :maketicket_vip
  end

  # Battle Of bands
  version :bob do
    process :maketicket_bob
  end

  def maketicket_bob
    second_image = MiniMagick::Image.open('http://localhost:3000/bob.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+330"
      end
    end
  end

  def maketicket_vip
    second_image = MiniMagick::Image.open('http://localhost:3000/vip.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+330"
      end
    end
  end


  def maketicket_day1
    second_image = MiniMagick::Image.open('http://localhost:3000/day1.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+130"
      end
    end

  end


  def maketicket_day2
    second_image = MiniMagick::Image.open('http://localhost:3000/day2.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+130"
      end
    end

  end



  def maketicket_season
    second_image = MiniMagick::Image.open('http://localhost:3000/season.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+130"
      end
    end

  end


  def maketicket_student_day1
    second_image = MiniMagick::Image.open('http://localhost:3000/day1_student.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+330"
      end
    end

  end

  def maketicket_student_day2
    second_image = MiniMagick::Image.open('http://localhost:3000/day2_student.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+330"
      end
    end

  end


  def maketicket_student_season
    second_image = MiniMagick::Image.open('http://localhost:3000/season_student.jpg')
    manipulate! do |img|
      second_image.composite(img) do |c|
        c.compose "Over" # OverCompositeOp
        # c.gravity "Northeast" # copy second_image onto first_image from (20, 20)
        c.geometry "+1575+330"
      end
    end

  end
end
