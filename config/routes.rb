Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'project-play' => 'home#projectplay', as: 'projectplay'

  get 'tos' => 'home#termsandconditions', as: 'tos'
  get 'genqr' => 'home#generateqr'
  get 'trendsgame' => 'home#game', as: 'game'
  post 'home/activate/:id' => 'home#activate'

  get 'tickets_day1' => 'tickets#adult_day_one'

  get 'tickets_day2' => 'tickets#adult_day_two'

  get 'tickets_season' => 'tickets#adult_season'

  get 'tickets_season_student' => 'tickets#student_season'


  get 'tickets_student_day1' => 'tickets#student_day1'

  get 'tickets_student_day2' => 'tickets#student_day2'

  get 'tickets_vip' => 'tickets#vip_tickets'

  get 'tickets_bob' => 'tickets#bob_tickets'

  get 'checkticket/:code' => 'tickets#ticket_check'

  get 'validateticket/:code' => 'tickets#ticket_verify'


  resources :tickets, only: [:create, :new, :index]

  # get 'driftgame' => 'home#casino'
  # get 'importtickets' => 'tickets#import', as: 'import_get'
  post 'importtickets' => 'tickets#import', as: 'import'
end
