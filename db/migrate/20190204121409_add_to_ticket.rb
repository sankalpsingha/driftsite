class AddToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :ticket_type, :integer
    add_column :tickets, :number_of_tickets, :integer
    add_column :tickets, :student, :integer
  end
end
