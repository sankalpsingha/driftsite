class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :random_code
      t.string :user_listed
      t.string :qr_code

      t.timestamps
    end
  end
end
