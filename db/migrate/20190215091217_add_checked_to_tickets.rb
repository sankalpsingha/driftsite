class AddCheckedToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :checked, :boolean, default: false
  end
end
